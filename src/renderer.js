const ejs = require('ejs');
const moment = require('moment');
const path = require('path');

class Renderer {
  constructor(params) {
    this.ejsPath = path.join(process.cwd()+'/src/views/index.ejs');;
    this.params = params || {};
  }

  static helpers() {
    return { moment };
  }


  async styles() {
    const style = await promisify(sass.render)({
      file: this.stylesPath,
      includePaths: ['assets/styles'],
    })
    return style.css.toString('utf-8')
  }

  async render() {
    const params = {
      ...this.params,
      helpers: Renderer.helpers(),
      styles: await this.styles(),
    }
    const templateStr = ejs.fileLoader(this.ejsPath, 'utf8');
    const template = ejs.compile(templateStr, { filename: this.ejsPath });
    return template(params);
  }
}

module.exports = Renderer

