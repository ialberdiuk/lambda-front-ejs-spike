# AWS Lambda + EJS + SCSS

This is a spike repo to show how to render HTML and CSS from a lambda function. This is one of the best ways to easily render front end with a decent performance as SCSS (CSS), JS uilities, etc.. are passed as params and there is not need to call s3 or other lambda functions (or any other endpoints to retrieve static resources).

Anyway, as node-sass can be a little bit problematic depending on your OS, I left boostrap in the headers of the index.ejs file, just in case you face any issue (it requires same OS, it means if you are developing in Win or Mac you will face issues when you deploy your lambda function manually via zip with node_modules included).

With node-sass all styles are embedded into one single HTML file. This makes it easy to deploy and share with others.

Example below do not use node-sass I just linked bootstrap. Deploying with codepipeline and a proper buildspec I would make sure the installation of node-sass runs smoothly. As this is only a POC should be enough if you want to show how to render front from a lambda function.

Click [here](https://68eaiuhet5.execute-api.us-east-1.amazonaws.com/dev) to navigate to deployed POC

https://68eaiuhet5.execute-api.us-east-1.amazonaws.com/dev


Probably, you should be thinking that this is not the best way for performance reasons and I agreee 100%. A gambling company would never use lambdas to render front because of cold start and other reasons. However if you need to deploy a small document about the integration of one of your systems or any other stuff for your internal company use case, this way is an acceptable option.

Last but not least, if you want to deploy via cloudformation and to use SAM, you need to change the way to get the path.

```
path.join(process.cwd) by path.join(__dirname)

```

## Running it

```bash
npm install
npm bin/render.js > out.html
```

## Tests

To run automated tests simply run:

```
npm test
```

## Deployment on AWS Lambda

To deploy on aws lambda you have to use AWS-CLI:

```
zip -r spike.zip ./*
aws lambda update-function-code --function-name FUNCTION_NAME --zip-file fileb://spike.zip
rm spike.zip
```

Make sure your aws user has correct rights. Here is an IAM policy which you can use or attach all exec lambda policy to your user:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "lambda:UpdateFunctionCode",
                "lambda:UpdateFunctionConfiguration", 
                "lambda:InvokeFunction",
                "lambda:GetFunction"
            ],
            "Resource": ["ARN_ADDRESS_OF_YOUR_LAMBDA_FUNCTION"]
        }
    ]
}

```

To test uploaded function run:

```
aws lambda invoke --function-name FUNCTION_NAME index.html
cat index.html
```
I created a user     and I attached all possible permissions to work with lambdas
